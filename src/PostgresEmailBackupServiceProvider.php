<?php

namespace ppowalski\postgresEmailBackup;

use Illuminate\Support\ServiceProvider;
use ppowalski\postgresEmailBackup\Console\Commands\Backup;

class PostgresEmailBackupServiceProvider extends ServiceProvider
{
    public function boot() {
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'prostgresEmailBackup');

        $this->commands([
            Backup::class
        ]);

        $this->publishes([
            __DIR__ . '/config/prostgresEmailBackup.php' => config_path('prostgresEmailBackup.php'),
        ], 'prostgresEmailBackup');
    }
}
