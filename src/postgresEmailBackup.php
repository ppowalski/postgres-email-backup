<?php

namespace ppowalski\postgresEmailBackup;

use Illuminate\Support\Facades\Mail;

/**
 * Class postgresEmailBackup
 * @package ppowalski\postgresEmailBackup
 */
class postgresEmailBackup
{
     public function createAndSendBackup(): void
     {
          $host = env('DB_HOST', null);
          $port = env('DB_PORT', null);
          $username = env('DB_USERNAME', null);
          $password = env('DB_PASSWORD', null);
          $dbName = env('DB_DATABASE', null);
          $date = date('Y_m_d_H_i_s');

          $command = config('prostgresEmailBackup.pg_exec_command', 'pg_dump');
          $emails = config('prostgresEmailBackup.backup_emails', []);
          $fileName = config('prostgresEmailBackup.file_name', "dbbackup_{$date}.sql");
          $subject = config('prostgresEmailBackup.email_subject', "Backup");

          if(!empty($host) && !empty($port) && !empty($username)
          && !empty($password) && !empty($dbName)) {
               exec("{$command} --dbname=postgresql://{$username}:{$password}@{$host}:{$port}/{$dbName} > {$fileName}",$output);

               if(empty($output) && !empty($emails)) {
                   Mail::send('prostgresEmailBackup::emailTemplate', [], function ($message) use ($emails, $subject, $fileName) {
                        $message->from(env('MAIL_FROM_ADDRESS'), 'Backup system');
                        $message->subject($subject);
                        $message->priority(1);

                        $this->addBackupToArchive($fileName);
                        $message->attach($fileName . '.zip');

                        foreach($emails as $email) {
                             $message->bcc($email);
                        }
                   });

                        if(file_exists($fileName . '.zip')) {
                             unlink($fileName . '.zip');
                        }
               }
          }
     }

     private function addBackupToArchive(string $fileName)
     {
          $zip = new \ZipArchive();

          if ($zip->open($fileName . '.zip', \ZipArchive::CREATE) === TRUE) {
               $zip->addFile($fileName, 'backup.sql');
               $zip->close();

               if(file_exists($fileName)) {
                    unlink($fileName);
               }
          }
     }
}
