<?php

namespace ppowalski\postgresEmailBackup\Console\Commands;

use Illuminate\Console\Command;
use ppowalski\postgresEmailBackup\postgresEmailBackup;

class Backup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email-backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create and send the postgres database backup';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
         $postgresEmailBackup = new postgresEmailBackup();
         $postgresEmailBackup->createAndSendBackup();
    }
}
