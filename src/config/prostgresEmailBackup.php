<?php

return [
    'pg_exec_command' => 'pg_dump',

    'backup_emails' => [
        'mail@example.com'
    ],

    'email_subject' => env('APP_NAME') . ' backup - ' . date('Y-m-d'),

    'file_name' => "backups\dbbackup_". date('Y_m_d_H_i_s') .".sql",

    'archive_password' => ''
];
