<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Email backup system</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    </head>

    <body style="font-family: 'Nunito', sans-serif; margin:0; padding:0; background-color: #e0e0e0;">
        <header style="text-align: center;font-size: 32px;background-color: #af1515; color: #FFF !important; padding: 10px;">
            Email backup system

            <div style="font-size: 18px; color: #FFF !important;">
                Date: {{ date('Y-m-d') }}
            </div>

            <div style="font-size: 10px; margin-top: 10px; color: #FFF !important;">
                Copyright &copy; Patryk Powalski. All right reserved!!!
            </div>
        </header>

        <h4 style="padding: 0 10px; text-align: center; color: #000 !important;">
            The backup is included in the attachment.
        </h4>
    </body>
</html>
